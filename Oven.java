import java.util.Scanner; 
public class Oven{
	
	private String brand;
	private int heat;
	private int serialNumber;
	
	public Oven(String brand, int heat, int serialNumber){
		this.brand = brand;
		this.heat = heat;
		this.serialNumber = serialNumber;
	}
	
	
	public void preheat(){
		System.out.println("Oven set to preheat untill " +heat+ " degrees");
	}
	
	public int getHeat(){
		return this.heat;
	}
	
	public void setHeat(int newHeat){
		this.heat = newHeat;
	}
	
	public void serialNumber(int userInput){
		int validValue = validateInput(userInput);
		this.serialNumber = userInput;
	}
	
	public int getSerialNumber(){
		return this.serialNumber;
	}
	
	public void setSerialNumber(int newSerialNumber){
		this.serialNumber = newSerialNumber;
	}
	
	public String getBrand(){
		return this.brand;
	}
	
	public void setBrand(String newBrand){
		this.brand = newBrand;
	}
	
	public int validateInput(int input){
		Scanner sc = new Scanner(System.in);
		if(input > 0){
			return input;
		}
		
		do{
			System.out.println("Please input a POSITIVE number");
			input = sc.nextInt();
		}
		while(input > 0);
		return input;
	}
	
	
	
}