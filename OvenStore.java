import java.util.Scanner;
public class OvenStore{
	
	public static void main (String[]args){
		Scanner sc = new Scanner(System.in);
		Oven[] ovens = new Oven[4];
		
		for(int i = 0; i < ovens.length; i++){
			
			System.out.println("Enter oven brand");
			String brand  = sc.nextLine();
			System.out.println("Set timer");
			int serialNumber = sc.nextInt();
			System.out.println("Set Temperature");
			int heat= sc.nextInt();
			sc.nextLine();
			ovens[i] = new Oven(brand, heat, serialNumber);
		}
		
		System.out.println(ovens[3].getBrand());
		System.out.println(ovens[3].getSerialNumber());
		System.out.println(ovens[3].getHeat());
		
		
		
		System.out.println("Oven modifications");
		
		System.out.println("What will the new brand be?");
		String betterBrand = sc.nextLine();
		ovens[3].setBrand(betterBrand);
		
		System.out.println("What will the new serialNumber be?");
		int differentSerialNumber = sc.nextInt();
		ovens[3].setSerialNumber(differentSerialNumber);
		
		System.out.println("What will the new heat be?");
		int betterHeat = sc.nextInt();
		ovens[3].setHeat(betterHeat);
		
		System.out.println(ovens[3].getBrand());
		System.out.println(ovens[3].getSerialNumber());
		System.out.println(ovens[3].getHeat());
		
		ovens[0].preheat();
		}
}